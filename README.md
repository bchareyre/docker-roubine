# My docker images
[![images](https://img.shields.io/badge/dynamic/json?label=Number%20of%20images&query=%24.length&url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fapi%2Fv4%2Fprojects%2F14893%2Fregistry%2Frepositories&logo=docker&style=for-the-badge)](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/docker/container_registry)
[![pipeline status](https://img.shields.io/gitlab/pipeline-status/roubine/docker?gitlab_url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr&style=for-the-badge)](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/docker/-/commits/main)

Provides some services based on a customized base image.
Current services:
- jupyter notebooks local server
- latex engine

## main image
Image link
```
gricad-registry.univ-grenoble-alpes.fr/roubine/docker
```
- base system: `debian:buster`
- workdir: `/work`
- dependencies installed: `build-essential init python3-dev python3-venv git emacs-nox`
- python virtual: `/home/.venv`
- python modules: `autopep8 requests urllib3`

Run simple temporary container with bash
```bash
docker run --rm -it gricad-registry.univ-grenoble-alpes.fr/roubine/docker
```

## jupyter notebook
Image link
```
gricad-registry.univ-grenoble-alpes.fr/roubine/docker/jupyter
```

- python modules:
    - science: `numpy scipy sympy matplotlib`
    - jupyter: `jupyter jupyter_contrib_nbextensions jupyter_nbextensions_configurator`

Run a temporary jupyter notebook server binding a host folder:
- host folder path: `/home/user/myfolder`
```bash
docker run --rm -it -p 8888:8888 -v /home/user/myfolder:/work gricad-registry.univ-grenoble-alpes.fr/roubine/docker/jupyter
```

## latex engine
Image link
```
gricad-registry.univ-grenoble-alpes.fr/roubine/docker/latex
```

- additional dependencies: `texlive-full`

Compile a latex document on the host with pdflatex:
- host folder: `/home/user/myfolder`
- host latex document: `/home/user/myfolder/mydocument.tex`

```bash
docker run --rm -it -v /home/user/myfolder:/work gricad-registry.univ-grenoble-alpes.fr/roubine/docker/latex pdflatex mydocument.tex
```
or
```bash
docker run --rm -it -v /home/user/myfolder:/work -e TEX_FILE=mydocument.tex gricad-registry.univ-grenoble-alpes.fr/roubine/docker/latex
```
