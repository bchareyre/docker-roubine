FROM debian:buster
LABEL org.opencontainers.image="emmanuel.roubin@univ-grenoble-alpes.fr"

# environnement variables (full scope available on next builds)
ENV USER=glorfindel
ENV HOME=/home/${USER}
ENV UID=1000
ENV WORK=/work
ENV VIRTUAL_ENV=${HOME}/.venv
ENV PATH="${VIRTUAL_ENV}/bin:${PATH}"

# install system dependencies & create user
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -y apt-utils && \
    apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
    build-essential tini \
    python3-dev python3-venv \
    git emacs-nox && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    useradd ${USER} -M -d ${HOME} -s /bin/bash -u ${UID} && \
    mkdir ${WORK}


# copy configuration files
COPY .bashrc .emacs ${HOME}/

# python virtual env
RUN python3 -m venv $VIRTUAL_ENV && \
    pip install -U pip && \
    pip install requests urllib3

# fix permissions and change user
RUN chown -R ${USER}:${USER} ${HOME} && \
    chown -R ${USER}:${USER} ${WORK}

# non root user & workdir
USER ${USER}
WORKDIR ${WORK}

# init
CMD ["bash"]
